#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_i2c.h"
#include "string.h"
#include "stdlib.h"
#include "eeprom/re_app_eeprom.h"

extern void RE_NVS_WriteStruct(uint16_t MemAddress, System_Config_t *pSystem_Config);
extern void RE_NVS_ReadStruct(uint16_t MemAddress);
extern void RE_NVS_WriteByte(uint16_t MemAddress, uint8_t Data, uint8_t Offset);
extern uint8_t RE_NVS_ReadByte(uint16_t MemAddress, uint8_t Offset);
extern void RE_NVS_WriteString(uint16_t MemAddress, char * pData, uint8_t Offset, uint8_t DataLength);
extern uint8_t * RE_NVS_ReadString(uint16_t MemAddress, uint8_t Offset, uint8_t DataLength);
extern void RE_NVS_WriteArray(uint16_t MemAddress, uint8_t *pData,uint16_t TxBufferSize);
extern uint8_t * RE_NVS_ReadArray(uint16_t MemAddress, uint16_t RxBufferSize);