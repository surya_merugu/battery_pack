/**
  ******************************************************************************
  * @file           : eeprom.c
  * @brief          : EEPROM peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "eeprom.h"
#include "limits.h"

extern I2C_HandleTypeDef hi2c1;
static __IO uint32_t t_ms;
static __IO uint8_t rx_cplt = 0;

#if 0
uint8_t *RE_EEPROM_GetPhyId(void)
{
    static uint8_t buffer[PHY_ID_LEN];
    RE_EEPROM_Read(buffer, PHY_ID_LEN, PHY_ID_ADDR);
    return buffer;
}

void RE_EEPROM_PutPhyId(void *p_physical_id)
{
    RE_EEPROM_Write(p_physical_id, PHY_ID_LEN, PHY_ID_ADDR);
}

uint8_t *RE_EEPROM_Get_FwUpgradePend(void)
{
    static uint8_t buffer;
    RE_EEPROM_Read(&buffer, FW_UPGRADE_PEND_LEN, FW_UPGRADE_PEND_ADDR);
    return &buffer;
}

void RE_EEPROM_Put_FwUpgradePend(uint8_t *p_fw_upgrade_pend)
{
    RE_EEPROM_Write(p_fw_upgrade_pend, FW_UPGRADE_PEND_LEN, FW_UPGRADE_PEND_ADDR);
}

uint8_t *RE_EEPROM_GetCanId(void)
{
    static uint8_t buffer[CAN_ID_LEN];
    RE_EEPROM_Read(buffer, CAN_ID_LEN, CAN_ID_ADDR);
    return buffer;
}

void RE_EEPROM_PutCanId(uint8_t *p_can_id)
{
    RE_EEPROM_Write(p_can_id, CAN_ID_LEN, CAN_ID_ADDR);
}

uint8_t *RE_EEPROM_GetLoc(void)
{
    static uint8_t buffer;
    RE_EEPROM_Read(&buffer, BAT_LOC_LEN, BAT_LOC_ADDR);
    return &buffer;
}

void RE_EEPROM_PutLoc(uint8_t *p_bat_loc)
{
    RE_EEPROM_Write(p_bat_loc, BAT_LOC_LEN, BAT_LOC_ADDR);
}

uint8_t *RE_EEPROM_GetRcpsMode(void)
{
    static uint8_t buffer[RCPS_MODE_LEN];
    RE_EEPROM_Read(buffer, RCPS_MODE_LEN, RCPS_MODE_ADDR);
    return buffer;
}

void RE_EEPROM_PutRcpsMode(uint8_t *p_rcps_mode)
{
    RE_EEPROM_Write(p_rcps_mode, RCPS_MODE_LEN, RCPS_MODE_ADDR);
}

uint8_t *RE_EEPROM_GetFwVersion(void)
{
    static uint8_t buffer[FW_VERSION_LEN];
    RE_EEPROM_Read(buffer, FW_VERSION_LEN, FW_VERSION_ADDR);
    return buffer;
}

void RE_EEPROM_PutFwVersion(uint8_t *p_fw_version)
{
    RE_EEPROM_Write(p_fw_version, FW_VERSION_LEN, FW_VERSION_ADDR);
}

uint8_t *RE_EEPROM_GetG2G(void)
{
    static uint8_t g2g_status;
    RE_EEPROM_Read(&g2g_status, G2G_STAT_LEN, G2G_STAT_ADDR);
    return &g2g_status;
}
void RE_EEPROM_PutG2G(uint8_t *p_g2g_status)
{
    RE_EEPROM_Write(p_g2g_status, G2G_STAT_LEN, G2G_STAT_ADDR);
}

uint8_t *RE_EEPROM_GetTamperStatus(void)
{
    static uint8_t tamper_status;
    RE_EEPROM_Read(&tamper_status, TAMPER_STAT_LEN, TAMPER_STAT_ADDR);
    return &tamper_status;
}
void RE_EEPROM_PutTamperStatus(uint8_t *p_tamper_status)
{
    RE_EEPROM_Write(p_tamper_status, TAMPER_STAT_LEN, TAMPER_STAT_ADDR);
}

uint8_t *RE_EEPROM_GetErrCode(void)
{
    static uint8_t buffer[ERR_CODES_LEN];
    RE_EEPROM_Read(buffer, ERR_CODES_LEN, ERR_CODES_ADDR);
    return buffer;
}

void RE_EEPROM_PutErrCode(uint8_t err_type, uint8_t *p_value, uint8_t len)
{
    uint16_t error_addr = (ERR_CODES_ADDR + err_type);
    RE_EEPROM_Write(p_value, len, error_addr);
}
#endif

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    __NOP();
}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    rx_cplt = 0x01;
    __NOP();
}


void RE_EEPROM_Write(uint8_t *p_data,  uint8_t len, uint16_t mem_address)
{
    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
    {
    }
    while ((HAL_GetTick() - t_ms) < 6)
    {
    }
    if (HAL_I2C_Mem_Write_DMA(&hi2c1, EEPROM_RW_ADDR, mem_address, MEM_ADD_SIZE, p_data, len) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    t_ms = HAL_GetTick();
}


#if 0
void RE_EEPROM_Write(uint8_t *p_data, uint8_t len, uint16_t mem_address)
{
    uint8_t writeData[16] = {0};
    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
    {
    }
    while ((HAL_GetTick() - t_ms) < 6)
    {
    }
    
    writeData[0] = (uint8_t) ((mem_address & 0xFF00) >> 8);
    writeData[1] = (uint8_t) (mem_address & 0xFF);

    /* And copy the content of the pData array in the temporary buffer */
    memcpy(writeData+2, p_data, len);
    
    /* Write Memory address + DataBytes */
    if(HAL_I2C_Master_Transmit(&hi2c1, EEPROM_RW_ADDR, writeData, len+2, HAL_MAX_DELAY) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    
    /* Wait for Successful write */
    while(HAL_I2C_Master_Transmit(&hi2c1, EEPROM_RW_ADDR, 0, 0, HAL_MAX_DELAY) != HAL_OK);
    t_ms = HAL_GetTick();
    
}
#endif

void RE_EEPROM_Read(uint8_t *p_data,  uint8_t len, uint16_t mem_address)
{
    rx_cplt = 0x00;
    while ((HAL_GetTick() - t_ms) < 6)
    {
    }
    if (HAL_I2C_Mem_Read_DMA(&hi2c1, EEPROM_RW_ADDR, mem_address, MEM_ADD_SIZE, p_data, len) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    t_ms = HAL_GetTick();
    /* Wait for the data to be received */
    while (!rx_cplt)
    {
    }
}
