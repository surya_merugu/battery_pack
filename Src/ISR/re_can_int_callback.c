/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_can_int_callback.c
  * Origin Date           :   14/04/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISC
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, APR 2020
  *****************************************************************************
  */

/* Includes */
#include "main/main.h"
#include "re_can_int_callback.h"

extern CAN_RxHeaderTypeDef RxHeader;
extern CAN_TxHeaderTypeDef TxHeader;
extern CAN_HandleTypeDef hcan1_t;
extern uint32_t Self_CANID;
extern I2C_HandleTypeDef hi2c1_t;
extern System_Config_t System_Config;
uint8_t can_kit_rcvd_msg[8];
uint8_t kit_rx_can_flag = 0;
    
/**
  ==============================================================================
                          ##### Callback functions #####
  ==============================================================================
    [..]
    This subsection provides the following callback functions:
      ( ) HAL_CAN_TxMailbox0CompleteCallback
      ( ) HAL_CAN_TxMailbox1CompleteCallback
      ( ) HAL_CAN_TxMailbox2CompleteCallback
      ( ) HAL_CAN_TxMailbox0AbortCallback
      ( ) HAL_CAN_TxMailbox1AbortCallback
      ( ) HAL_CAN_TxMailbox2AbortCallback
      (+) HAL_CAN_RxFifo0MsgPendingCallback
      ( ) HAL_CAN_RxFifo0FullCallback
      ( ) HAL_CAN_RxFifo1MsgPendingCallback
      ( ) HAL_CAN_RxFifo1FullCallback
      ( ) HAL_CAN_SleepCallback
      ( ) HAL_CAN_WakeUpFromRxMsgCallback
      ( ) HAL_CAN_ErrorCallback
  ==============================================================================
*/
volatile uint32_t can_err_code = 0;
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
  uint8_t rcvd_msg[8];
  if(HAL_CAN_GetRxMessage(&hcan1_t, CAN_RX_FIFO0, &RxHeader, rcvd_msg) != HAL_OK)
  {
    RE_Error_Handler(__FILE__, __LINE__);
  }
  /**Handle kit Messages**/
  if (RxHeader.ExtId >= 0xC9 && RxHeader.ExtId <= 0xFA)
  {
      memcpy(can_kit_rcvd_msg, rcvd_msg, 8); 
      kit_rx_can_flag = 1;
  } 
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan)
{
    if (hcan->ErrorCode != 0)
    {
        
    }
}


/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/
