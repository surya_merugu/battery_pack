/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_wwdg_init_callback.c
  * Origin Date           :   13/05/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, MAY 2020
  *****************************************************************************
  */

/* Includes */
#include "re_wwdg_int_callback.h"

extern WWDG_HandleTypeDef hwwdg_t;


__stackless void HAL_WWDG_EarlyWakeupCallback(WWDG_HandleTypeDef * hwwdg)
{
    if (hwwdg -> Instance == WWDG)
    {
       
    }   

}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/