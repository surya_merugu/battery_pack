/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_adc_init_callback.c
  * Origin Date           :   14/03/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, APR 2020
  *****************************************************************************
  */

/* Includes */
#include "re_adc_int_callback.h"
#include "math.h"

extern ADC_HandleTypeDef hadc1_t;
extern uint8_t Temperature_Flag;
extern uint8_t Tamper_Flag;
extern gaugeData_t gaugeData;
extern uint32_t Temperature;

/**
 * @brief ADC conversion complete callback
 * this function handles the interupt callbacks of ADC
 * @param hadc1_t ADC handler typedef
 * @retval None
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    if(Temperature_Flag == 1)
    {
        Temperature = HAL_ADC_GetValue(&hadc1_t);
//        Temperature = HAL_ADC_GetValue(&hadc1_t);
        gaugeData.ExternalTemperature = (uint16_t)((-44.82 * log(4096 - Temperature) + 365.250)*100);
        Temperature_Flag = 0;
        if(HAL_ADC_Stop_IT(&hadc1_t) != HAL_OK)
        {
            RE_Error_Handler(__FILE__, __LINE__);
        }
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/