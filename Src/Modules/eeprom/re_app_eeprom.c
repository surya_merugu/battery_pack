/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_app_eeprom.c
  * Origin Date           :   20/04/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISC
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, APR 2020
  *****************************************************************************
  */

/* Includes */
#include "eeprom/re_app_eeprom.h"

#define EEPROM_ADDRESS 0xA0

extern I2C_HandleTypeDef hi2c1_t;

System_Config_t System_Config;
uint32_t Self_CANID;
uint8_t CAN_Id[4];
System_Config_t *pSystem_Config;
uint8_t I2C_StructReceiveBuffer[sizeof(System_Config)];

RE_StatusTypeDef RE_NVS_ReadSysConfig(void)
{
    uint16_t sizeOfStruct = sizeof(System_Config);
    if(HAL_I2C_Mem_Read (&hi2c1_t, EEPROM_ADDRESS, 0, 0xFFFF, I2C_StructReceiveBuffer, sizeOfStruct, 1000) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    HAL_Delay(100);
    System_Config.FW_Version_Info = I2C_StructReceiveBuffer[0];
    System_Config.Can_ID = (I2C_StructReceiveBuffer[1] << 16) | (I2C_StructReceiveBuffer[2] << 24) |\
                             (I2C_StructReceiveBuffer[3] << 8) | I2C_StructReceiveBuffer[4];
    System_Config.Physical_ID[0]          = I2C_StructReceiveBuffer[5];
    System_Config.Physical_ID[1]          = I2C_StructReceiveBuffer[6];
    System_Config.Physical_ID[2]          = I2C_StructReceiveBuffer[7];
    System_Config.Physical_ID[3]          = I2C_StructReceiveBuffer[8];
    System_Config.Physical_ID[4]          = I2C_StructReceiveBuffer[9];
    System_Config.Physical_ID[5]          = I2C_StructReceiveBuffer[10];
    System_Config.Physical_ID[6]          = I2C_StructReceiveBuffer[11];
    System_Config.G2G                     = I2C_StructReceiveBuffer[12];
    System_Config.Charge_FET_Status       = I2C_StructReceiveBuffer[13];
    System_Config.Discharge_FET_Status    = I2C_StructReceiveBuffer[14];
    System_Config.Alert_Flag              = I2C_StructReceiveBuffer[15];
    System_Config.Bat_err                 = I2C_StructReceiveBuffer[16];
    System_Config.Soc_err                 = I2C_StructReceiveBuffer[17];
    System_Config.CAN_err                 = I2C_StructReceiveBuffer[18];
    System_Config.Tamper_err              = I2C_StructReceiveBuffer[19];
    System_Config.Temp_err                = I2C_StructReceiveBuffer[20];
    System_Config.Latch_err               = I2C_StructReceiveBuffer[21];
    System_Config.FET_err                 = I2C_StructReceiveBuffer[22];
    return RE_OK;
}

RE_StatusTypeDef RE_NVS_GetCanID(void)
{
    if(HAL_I2C_Mem_Read (&hi2c1_t, EEPROM_ADDRESS, 1, 0xFFFF, CAN_Id, 4, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }  
    Self_CANID = (CAN_Id[1] << 8) | (CAN_Id[0]) | (CAN_Id[2] << 16) | (CAN_Id[3] << 24);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/