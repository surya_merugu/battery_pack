/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_location.c
  * Origin Date           :   25/05/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, MAY 2020
  *****************************************************************************
  */

/* Includes */
#include "location/re_app_location.h"


RE_StatusTypeDef RE_ToggleGPIO(void)
{
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);
    return RE_OK;
}


/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/