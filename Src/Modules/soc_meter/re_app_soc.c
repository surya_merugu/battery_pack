/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_app_soc.c
  * Origin Date           :   14/04/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   bq34z100
  * Target                :   STM32F407-DISC
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, APR 2020
  *****************************************************************************
  */

/* Includes */
#include "soc_meter/re_app_soc.h"

#define numOfCanPackets  6
#define sizeOfCanPacket  7

uint8_t gaugeDataPacket[numOfCanPackets][sizeOfCanPacket];
gaugeData_t gaugeData;
DataBlock_config_t Block;                     // Data Block config structure to read SoC-meter data in blocks format
uint8_t *pGaugeData = NULL;

extern I2C_HandleTypeDef hi2c1_t;

static uint8_t  RE_SoC_ConfigDataBlock (uint8_t block_num);
static RE_StatusTypeDef RE_SoC_I2CReadDataBlock (uint8_t block_num);
static uint16_t RE_Byte_Swap (uint16_t msb, uint16_t lsb);
static RE_StatusTypeDef  RE_SoC_MapGaugeData (uint8_t block_num);

static uint8_t RE_SoC_ConfigDataBlock(uint8_t block_num)
{
   switch(block_num)
   {
    case 0:
      /* Block: 0 */
      Block.Addr  = 0x02;
      Block.Len   = 18;
      break;
    case 1:
    /* Block: 1 */
      Block.Addr  = 0x18;
      Block.Len   = 8;
      break;
     case 2: 
    /* Block: 2 */
      Block.Addr  = 0x24;
      Block.Len   = 16;
      break;
    case 3:
    /* Block: 3 */
      Block.Addr  = 0x68;
      Block.Len   = 4;
      break;
    default :
      /**To Do**/
      break;
    }
    return Block.Len;
}

/**
  * @brief  Read Gauge data blocks into array
  * @param  block_num : Block Number
  * @retval None  
  */
static RE_StatusTypeDef RE_SoC_I2CReadDataBlock (uint8_t block_num)
{  
  RE_SoC_ConfigDataBlock(block_num);    /* Configure blocks first */  
  pGaugeData = malloc(Block.Len);    /* Allocate the memory bytes to array : pGaugeData */
  /* Master write address to Slave */
  if (HAL_I2C_Master_Transmit (&hi2c1_t, BQ_WR_ADDR, &Block.Addr, sizeof(Block.Addr), 1000)!=HAL_OK)
  {
    RE_Error_Handler(__FILE__, __LINE__);
  }
  /* Master request Slave for data */
  if (HAL_I2C_Master_Receive (&hi2c1_t, BQ_RD_ADDR, &pGaugeData[0], Block.Len, 1000)!=HAL_OK)
  {
    RE_Error_Handler(__FILE__, __LINE__);
  }  
 /* Map array data to structure */
  RE_SoC_MapGaugeData(block_num);
  /* Release memory allocated to PGaugeData */
  free(pGaugeData); 
  return RE_OK;
}

/**
  * @brief  Swap MSB and LSB 
  * @param  msb: Most Significant Byte
  * @param  lsb: Least Significant Byte
  * @retval swap_byte: Bytes after swapping  
  */
static uint16_t RE_Byte_Swap (uint16_t msb, uint16_t lsb)
{
  uint16_t swap_data;
  swap_data = ((lsb << 8) & (0xFF00))| ((msb) & (0x00FF)) ; 
  return swap_data;
}

/**
  * @brief  Map array data into Gauge_Param_t 
  * @param  block_num Block Number
  * @retval None  
  */
static RE_StatusTypeDef RE_SoC_MapGaugeData(uint8_t block_num)
{
        if (block_num == 0)
        {
        gaugeData.StateOfCharge        = (0x00 << 8) | (pGaugeData[0]);                  //0,[1:2]
        gaugeData.MaxError             = (0x00 << 8) | (pGaugeData[1]);                  //4, [1:2]  
        gaugeData.RemainingCapacity    = RE_Byte_Swap(pGaugeData[2], pGaugeData[3]);     //3,[5:6]
        gaugeData.FullChargeCapacity   = RE_Byte_Swap(pGaugeData[4], pGaugeData[5]);     //4, [3:4]
        gaugeData.Voltage              = RE_Byte_Swap(pGaugeData[6], pGaugeData[7]);     //0, [3:4]
        gaugeData.AvgCurrent           = RE_Byte_Swap(pGaugeData[8], pGaugeData[9]);     //1, [5:6]
        gaugeData.Temperature          = RE_Byte_Swap(pGaugeData[10], pGaugeData[11]);   //1, [3:4]
        gaugeData.Current              = RE_Byte_Swap(pGaugeData[14], pGaugeData[15]);   //0, [5:6]

        gaugeDataPacket[0][1]    = pGaugeData[0];   // 1. Soc
        gaugeDataPacket[0][2]    = 0;               
        gaugeDataPacket[0][3]    = pGaugeData[6];   // 2. Volatge
        gaugeDataPacket[0][4]    = pGaugeData[7];
        gaugeDataPacket[0][5]    = pGaugeData[14];  // 3. Current   // 4. Ext Temp1
        gaugeDataPacket[0][6]    = pGaugeData[15];
        gaugeDataPacket[1][3]    = pGaugeData[10];  // 5. Gauge Temperature
        gaugeDataPacket[1][4]    = pGaugeData[11];
        gaugeDataPacket[1][5]    = pGaugeData[8];   // 6. Avg Current
        gaugeDataPacket[1][6]    = pGaugeData[9];
        gaugeDataPacket[4][1]    = pGaugeData[1];   // 13. Max Error
        gaugeDataPacket[4][2]    = 0;
        gaugeDataPacket[3][5]    = pGaugeData[2];   // 12. Rem Cap
        gaugeDataPacket[3][6]    = pGaugeData[3];
        gaugeDataPacket[4][3]    = pGaugeData[4];   // 14. Full Chg Cap
        gaugeDataPacket[4][4]    = pGaugeData[5];
    }
    
   else if (block_num == 1)
    {
      gaugeData.AvgTimeToEmpty       = RE_Byte_Swap(pGaugeData[0], pGaugeData[1]);  //2,[3:4]
      gaugeData.AvgTimeToFull        = RE_Byte_Swap(pGaugeData[2], pGaugeData[3]);  //2,[1:2]
      gaugeData.PassedCharge         = RE_Byte_Swap(pGaugeData[4], pGaugeData[5]);  

      gaugeDataPacket[2][3]   = pGaugeData[0];    // 8. Avg Time to Empty
      gaugeDataPacket[2][4]   = pGaugeData[1];
      gaugeDataPacket[2][1]   = pGaugeData[2];    // 7. Avg Time to Full
      gaugeDataPacket[2][2]   = pGaugeData[3];

    }
    else if (block_num == 2)
    {
      gaugeData.AvailableEnergy      = RE_Byte_Swap(pGaugeData[0], pGaugeData[1]);    //2,[5:6]
      gaugeData.AvgPower             = RE_Byte_Swap(pGaugeData[2], pGaugeData[3]);    //3,[1:2]
      gaugeData.SerialNumber         = RE_Byte_Swap(pGaugeData[4], pGaugeData[5]);    //
      gaugeData.InternalTemperature  = RE_Byte_Swap(pGaugeData[6], pGaugeData[7]);    //3,[3:4]
      gaugeData.CycleCount           = RE_Byte_Swap(pGaugeData[8], pGaugeData[9]);    //4,[5:6]
      gaugeData.StateOfHealth        = RE_Byte_Swap(pGaugeData[10], pGaugeData[11]);  //5,[1:2]

      gaugeDataPacket[2][5]  = pGaugeData[0];    // 9. Avl Energy
      gaugeDataPacket[2][6]  = pGaugeData[1];
      gaugeDataPacket[3][1]  = pGaugeData[2];    // 10. Avg Pwr
      gaugeDataPacket[3][2]  = pGaugeData[3];
      gaugeDataPacket[3][3]  = pGaugeData[6];    // 11. Int Temp
      gaugeDataPacket[3][4]  = pGaugeData[7];
      gaugeDataPacket[4][5]  = pGaugeData[8];    // 15. Cycles
      gaugeDataPacket[4][6]  = pGaugeData[9];
      gaugeDataPacket[5][1]  = pGaugeData[10];   // 16. SoH
      gaugeDataPacket[5][2]  = pGaugeData[11];
    }
    else if (block_num == 3)
    {
      gaugeData.TrueRC       = RE_Byte_Swap(pGaugeData[0], pGaugeData[1]); //5,[3:4]
      gaugeData.TrueFCC      = RE_Byte_Swap(pGaugeData[2], pGaugeData[3]); //5,[5,6]
      gaugeDataPacket[5][3]  = pGaugeData[0];   // 17. TrueRC
      gaugeDataPacket[5][4]  = pGaugeData[1];
      gaugeDataPacket[5][5]  = pGaugeData[2];   // 18. TrueFCC
      gaugeDataPacket[5][6]  = pGaugeData[3];

    }
    else 
    {
      __NOP();
    }
    return RE_OK;
}

RE_StatusTypeDef RE_SoC_ReadGaugeData (void)
{

      /* Read data block 0 from bq34z100 */
      RE_SoC_I2CReadDataBlock(0);

      /* Read data block 1 from bq34z100 */
      RE_SoC_I2CReadDataBlock(1);
    
      /* Read data block 2 from bq34z100 */
      RE_SoC_I2CReadDataBlock(2);
    
      /* Read data block 3 from bq34z100 */
      RE_SoC_I2CReadDataBlock(3);

      return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/