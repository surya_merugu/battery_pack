/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_app_can.c
  * Origin Date           :   14/04/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISC
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, APR 2020
  *****************************************************************************
  */

/* Includes */
#include "can/re_app_can.h"
#include "eeprom/re_app_eeprom.h"
#include "temperature/re_app_temperature.h"
#include "string.h"
#include "rcps/re_app_rcps.h"

#define EEPROM_ADDRESS 0xA0
#define numOfCanPackets  6
#define sizeOfCanPacket  7

extern uint8_t gaugeDataPacket[numOfCanPackets][sizeOfCanPacket];
extern uint32_t Self_CANID;
extern CAN_TxHeaderTypeDef TxHeader;
extern CAN_HandleTypeDef hcan1_t;
extern System_Config_t System_Config;
extern uint8_t can_kit_rcvd_msg[8];
extern RE_StatusTypeDef RE_SoC_ReadGaugeData (void);
extern RE_StatusTypeDef RE_ReadTemperatureSensorData (void);

RE_StatusTypeDef RE_SEND_Pack_Ids (void)
{
  uint8_t tx_msg[8];
  TxHeader.DLC   = 1;
  TxHeader.ExtId = Self_CANID;
  TxHeader.IDE   = CAN_ID_EXT;
  TxHeader.RTR   = CAN_RTR_DATA;
  uint32_t TxMailbox;
  tx_msg[0] = 0x02;
  TxHeader.DLC   = 8;
  memcpy(tx_msg + 1, (uint8_t *)&System_Config.Physical_ID, 7);
  if (HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader, tx_msg, &TxMailbox) != HAL_OK)
  {
    RE_Error_Handler(__FILE__, __LINE__);
  }  
  return RE_OK;
}

RE_StatusTypeDef RE_CAN_RespondData (void)
{ 
    gaugeDataPacket[0][0]    = 0x10;    // 1st packet message ID
    gaugeDataPacket[1][0]    = 0x11;    // 2nd packet message ID
    gaugeDataPacket[2][0]    = 0x12;    // 3rd packet message ID
    gaugeDataPacket[3][0]    = 0x13;    // 4th packet message ID
    gaugeDataPacket[4][0]    = 0x14;    // 5th packet message ID
    gaugeDataPacket[5][0]    = 0x15;    // 6th packet message ID
    uint32_t TxMailbox;
    TxHeader.ExtId = Self_CANID;
    TxHeader.IDE   = CAN_ID_EXT;
    TxHeader.RTR   = CAN_RTR_DATA;
    TxHeader.DLC   = 7;
    uint8_t tx_msg[7];
    uint8_t numOfPackets = 6;
    for ( uint8_t i=0; i< numOfPackets; i++)
    {
        tx_msg[0]= gaugeDataPacket[i][0];
        for(uint8_t j=1; j<7; j++)
        {
            tx_msg[j] = gaugeDataPacket[i][j];
        }
        HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader, tx_msg, &TxMailbox);
        HAL_Delay(1);
    }
  return RE_OK;
}

RE_StatusTypeDef RE_CAN_Convkit_MsgHandler(void)
{
      uint32_t CanID;
      switch(can_kit_rcvd_msg[0])
      {  
      case 0x09:
        if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7))
        {
            RE_SEND_Pack_Ids();
        }
        break;
      case 0x5D:
        if((can_kit_rcvd_msg[1]) == 1)
        {
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_SET);  // PC9 = High : discharge inhibition ON
        }
        else if((can_kit_rcvd_msg[1]) == 0)
        {
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, GPIO_PIN_RESET);  // PC9 = Low : discharge inhibition OFF
        }
        if((can_kit_rcvd_msg[2]) == 1)
        {
            HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_SET);  // PD11 = High : Charge inhibition ON
        }
        else if((can_kit_rcvd_msg[2]) == 0)
        {
            HAL_GPIO_WritePin(GPIOD, GPIO_PIN_11, GPIO_PIN_RESET);  // PD11 = Low : Charge inhibition OFF
        }
        break;
      case 0x5E:
        RE_SoC_ReadGaugeData();
        RE_ReadTemperatureSensorData();
        gaugeDataPacket[1][1] = (gaugeData.ExternalTemperature) & 0xFF;
        gaugeDataPacket[1][2] = (gaugeData.ExternalTemperature >> 8) & 0xFF;
        memset(can_kit_rcvd_msg, 0, sizeof(can_kit_rcvd_msg));
        break;
      case 0x5F:
        CanID = (can_kit_rcvd_msg[1]) | ((can_kit_rcvd_msg[2]) << 8) | ((can_kit_rcvd_msg[3]) << 16) | ((can_kit_rcvd_msg[4]) << 24);
        if(Self_CANID == CanID)
        {            
            RE_CAN_RespondData();
        }
        memset(can_kit_rcvd_msg, 0, sizeof(can_kit_rcvd_msg));
        break;
      default :
        break;
      }
      return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/