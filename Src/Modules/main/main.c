/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   main.c
  * Origin Date           :   13/04/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, APR 2020
  *****************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main/main.h"

extern TIM_HandleTypeDef htim2_t;
extern bool TimerFlag;
extern uint8_t kit_rx_can_flag;
uint32_t pclk;

extern RE_StatusTypeDef RE_SystemClock_Config(void);
extern RE_StatusTypeDef RE_IWDG_Init(void);
extern RE_StatusTypeDef RE_CAN_Convkit_MsgHandler(void);
RE_StatusTypeDef RE_Location_GpioInit (void);


// This code was last programmed on 1-oct-2020
/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
      HAL_Init();
      RE_SystemClock_Config();
      RE_SoC_NVS_Init();
      RE_rcps_GpioInit();
      RE_NVS_GetCanID();
      RE_ADC1_Init();
      RE_DMA_Init();
      RE_CAN1_Init();
      RE_CAN1_Filter_Config();
      RE_CAN1_Start_Interrupt();
      RE_NVS_ReadSysConfig ();
      RE_Latch_GpioInit();
      RE_Location_GpioInit();
      RE_IWDG_Init();    
      RE_TIMER2_Init();
      pclk = HAL_RCC_GetPCLK1Freq();
//      RE_SoC_ReadGaugeData();
      if(HAL_TIM_Base_Start_IT(&htim2_t) != HAL_OK)
      {
          RE_Error_Handler(__FILE__, __LINE__);
      }    
      while (1)
      {
            if(TimerFlag == TRUE)
            {
                RE_TimerEventHandler ();
                TimerFlag = FALSE;
            }
            if (kit_rx_can_flag == 1)
            {
                RE_CAN_Convkit_MsgHandler();
                kit_rx_can_flag = 0;
            }
//            HAL_SuspendTick();
//            HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
//            HAL_ResumeTick();            
      }
}
/***************************** END OF FILE ************************************/