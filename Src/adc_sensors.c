/**
  ******************************************************************************
  * @file           : gpio.c
  * @brief          : GPIO peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "adc_sensors.h"
#include "math.h"

ADC_HandleTypeDef hadc1;

volatile uint16_t adc_dma_buf[ADC_SENSORS], adc_val[ADC_SENSORS];

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
void RE_ADC1_Init(void)
{

    /* USER CODE BEGIN ADC1_Init 0 */

    /* USER CODE END ADC1_Init 0 */

    ADC_ChannelConfTypeDef sConfig = {0};

    /* USER CODE BEGIN ADC1_Init 1 */

    /* USER CODE END ADC1_Init 1 */
    /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
    hadc1.Instance = ADC1;
    hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV6;
    hadc1.Init.Resolution = ADC_RESOLUTION_12B;
    hadc1.Init.ScanConvMode = ENABLE;
    hadc1.Init.ContinuousConvMode = ENABLE;
    hadc1.Init.DiscontinuousConvMode = DISABLE;
    hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc1.Init.NbrOfConversion = 5;
    hadc1.Init.DMAContinuousRequests = ENABLE;
    hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
    if (HAL_ADC_Init(&hadc1) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
    sConfig.Channel = ADC_CHANNEL_0;
    sConfig.Rank = 1;
    sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
    sConfig.Channel = ADC_CHANNEL_1;
    sConfig.Rank = 2;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
    sConfig.Channel = ADC_CHANNEL_2;
    sConfig.Rank = 3;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
    sConfig.Channel = ADC_CHANNEL_4;
    sConfig.Rank = 4;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
    sConfig.Channel = ADC_CHANNEL_5;
    sConfig.Rank = 5;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /* USER CODE BEGIN ADC1_Init 2 */
    HAL_ADC_Start_DMA(&hadc1, (uint32_t *)adc_dma_buf, 5);
    /* USER CODE END ADC1_Init 2 */
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
    /**ADC1 GPIO Configuration    
    PA0     ------> ADC1_IN0  LDR1_Pin
    PA1     ------> ADC1_IN1  LDR2_Pin
    PA2     ------> ADC1_IN2  LDR3_Pin
    PA4     ------> ADC1_IN4  TEMP1_Pin
    PA5     ------> ADC1_IN5  TEMP2_Pin
    */

    for (int i = 3; i < 5; i++)
    {
        if (i < 3)
        {
            adc_val[i] = adc_dma_buf[i]; /* store the values in adc_val from buffer */
        }
        else
        {
            adc_val[i] = (uint16_t)(round((-44.82 * log(4096 - adc_dma_buf[i]) + 365.250))); /* multiply with 100 to get upto 2 decimals */
        }
    }
}

uint16_t *RE_ADC_Get_TempSensor_Data(void)
{
    return (uint16_t *)adc_val;
}