/**
  ******************************************************************************
  * @file           : can.c
  * @brief          : CAN peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "can.h"
#include "string.h"

extern sys_var_t sys_var;
extern uint8_t socTrackingEnabled , SOC_TrackingData, Consumed_SOC;

CAN_HandleTypeDef hcan1;
CAN_TxHeaderTypeDef TxHeader;
CAN_RxHeaderTypeDef RxHeader;
Queue_t *p_CanRxQueue;
Queue_t *p_CanTxQueue;

static void RE_CAN1_Filter_Config(void);
static void RE_CAN1_Start_Interrupt(void);

uint8_t *p_can_soc_data;
uint16_t *p_can_adc_data;

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
void RE_CAN1_Init(void)
{
    /* USER CODE BEGIN CAN1_Init 0 */

    /* USER CODE END CAN1_Init 0 */

    /* USER CODE BEGIN CAN1_Init 1 */

    /* USER CODE END CAN1_Init 1 */
    hcan1.Instance = CAN1;
    hcan1.Init.Prescaler = 4;
    hcan1.Init.Mode = CAN_MODE_NORMAL;
    hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
    hcan1.Init.TimeSeg1 = CAN_BS1_13TQ;
    hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
    hcan1.Init.TimeTriggeredMode = DISABLE;
    hcan1.Init.AutoBusOff = ENABLE;
    hcan1.Init.AutoWakeUp = ENABLE;
    hcan1.Init.AutoRetransmission = ENABLE;
    hcan1.Init.ReceiveFifoLocked = DISABLE;
    hcan1.Init.TransmitFifoPriority = DISABLE;
    if (HAL_CAN_Init(&hcan1) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /* USER CODE BEGIN CAN1_Init 2 */
    RE_CAN1_Filter_Config();
    //    sys_var.dock_id = RE_EEPROM_GetCanId();
    p_CanRxQueue = RE_CreateQueue(RxQueueItems, RxQueueItemLen); /* A queue of {RxQueueItems} items of each {RxQueueItemLen} bytes in length */
    p_CanTxQueue = RE_CreateQueue(TxQueueItems, TxQueueItemLen); /* A queue of {TxQueueItems} items of each {TxQueueItemLen} bytes in length */
    RE_CAN1_Start_Interrupt();
    /* USER CODE END CAN1_Init 2 */
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan1)
{
    uint8_t queue_item[RxQueueItemLen];
    if (HAL_CAN_GetRxMessage(hcan1, CAN_RX_FIFO0, &RxHeader, &queue_item[5]) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /* Example ID = 0x1FA24305 */
    /* queue_item[3] = 0x1F */
    /* queue_item[2] = 0xA2 */
    /* queue_item[1] = 0x43 */
    /* queue_item[0] = 0x05 */
    queue_item[0] = (uint8_t)(RxHeader.ExtId >> 24);
    queue_item[1] = (uint8_t)(RxHeader.ExtId >> 16);
    queue_item[2] = (uint8_t)(RxHeader.ExtId >> 8);
    queue_item[3] = (uint8_t)(RxHeader.ExtId);
    queue_item[4] = RxHeader.DLC;
    if (((queue_item[1] == 0xB4) && (queue_item[2] == 0x00) && (queue_item[3] == 0x02))) /* ID: 0x00B40002 Dock ID is transmitted from frigg */
    {
        if (HAL_GPIO_ReadPin(LOC_GPIO_Port, LOC_Pin))
        {
            /* Message will be queued outside of the if condition */
            __NOP();
        }
        else
        {
            /* Discard the message */
            return;
        }
    }
    else if (((queue_item[1] == 0xE3) && (queue_item[3] == 0x00))) /* ID: 0x00E3xx00 Dock ID is transmitted from Bifrost */
    {
        if (HAL_GPIO_ReadPin(LOC_GPIO_Port, LOC_Pin))
        {
            /* Message will be queued outside of the if condition */
            __NOP();
        }
        else
        {
            /* Discard the message */
            return;
        }
    }    
    RE_EnQueue(p_CanRxQueue, queue_item, RxQueueItemLen);
}

uint8_t RE_CAN_EEPROM_Msg(uint8_t mode, uint8_t msg_id, uint8_t *p_data)
{
    /* mode => 1: write; 2: read */
    /* p_data[0] = DLC */
    /* p_data[1 : 8] = CAN msg */
    uint8_t mem_address = 0;
    switch (msg_id)
    {
    case 0x01: /*  Physical ID */
        mem_address = PHY_ID_ADDR;
        p_data[0] = PHY_ID_LEN;
        break;
    case 0x02: /* Dock ID and Host ID */
        mem_address = DOCK_ID_ADDR;
        p_data[0] = DOCK_ID_LEN;
        break;
    case 0x03: /* RCPS MODE */
        mem_address = RCPS_MODE_ADDR;
        p_data[0] = RCPS_MODE_LEN;
        break;
    case 0x04: /* FW VERSION */
        mem_address = FW_VERSION_ADDR;
        p_data[0] = FW_VERSION_LEN;
        break;
    case 0x05: /* G2G STATE */
        mem_address = G2G_STATE_ADDR;
        p_data[0] = G2G_STATE_LEN;
        break;
    case 0x06: /* TAMPER_STATE_ADDR */
        mem_address = TAMPER_STATE_ADDR;
        p_data[0] = TAMPER_STATE_LEN;
        break;
    case 0x07: /* Error Codes */
        mem_address = ERR_CODES_ADDR;
        p_data[0] = ERR_CODES_LEN;
        break;
    case 0x08: /* FW Update Pending flag */
        mem_address = FW_UPDATE_PEND_ADDR;
        p_data[0] = FW_UPDATE_PEND_LEN;
        break;
    case 0x09: /*store avl energy*/
        mem_address = AVL_ENERGY_ADDR;
        p_data[0] = AVL_ENERGY_LEN + EPOCH_TIME_LEN;
        break;
    case 0x0A:
        mem_address = HOST_VERIFICATION_ID_ADDR;
        p_data[0] = HOST_VERIFICATION_ID_LEN;        
        break;
    case 0x0B:
	mem_address = SOC_ADDR;
	p_data[0] = SOC_LEN;        
	break;	
    case 0x0C:
	mem_address = SOC_TRACKING_FLAG_ADDR;
	p_data[0] = SOC_TRACKING_FLAG_LEN;        
	break;
    default:
        p_data[0] = 0;
        return 0;
        break;
    }
    if (p_data[0] != 0)
    {
        switch (mode)
        {
        case EEPROM_WRITE:
            RE_EEPROM_Write(&p_data[1], p_data[0], mem_address);
            break;
        case EEPROM_READ:
            RE_EEPROM_Read(&p_data[1], p_data[0], mem_address);
            break;
        default:
            return 0;
            break;
        }
    }
    return 1;
}

void RE_CAN_FRIGG_Msg(uint8_t msg_id, uint8_t *p_data)
{
  
    /** Write initial values in EEPROM when Frigg frame is received */
   if(socTrackingEnabled != 1){
        
        socTrackingEnabled =1;
        static uint8_t buffer[3] = {0};
        // Read Gauge data and update to EEPROM and Also in global variable "SOC_TrackingData"
        if (RE_SOC_GetData(5, buffer) == 0x01) /* Read SOC */
        {
            Error_Handler(__FILE__, __LINE__);
        }
        SOC_TrackingData = buffer[1];
        Consumed_SOC = 0;
        
        buffer[0] = 0x01;  		/* SOC Tracking Flag */
        RE_EEPROM_Write(&buffer[0], 1, SOC_TRACKING_FLAG_ADDR); /* Write 1 byte */ 
        
        buffer[0] = SOC_TrackingData; 	/* SOC Avail Data */		/* Consumed SOC */	
        RE_EEPROM_Write(&buffer[0], 1, SOC_AVAIL_DATA_ADDR);  /* Write 2 bytes */
        
        buffer[0] = 0x00; 	/* Consumed SOC */	
        RE_EEPROM_Write(&buffer[0], 1, CONSUMED_SOC_ADDR);  /* Write 2 bytes */  
        HAL_GPIO_WritePin(CHG_INH_GPIO_Port, CHG_INH_Pin, GPIO_PIN_SET);
    }

    /* p_data[0] = DLC */
    /* p_data[1:8] = CAN data */
    switch (msg_id) 
    {
    case 0x00: /*  */
        /* Cause: Request system reset */
        /* Effect: perform soft reset on the system */
        HAL_NVIC_SystemReset();
        break;
    case 0x01:
        /* Cause: Request physical ID */
        /* Effect: Send battery physical ID to FRIGG */
        if (RE_CAN_EEPROM_Msg(EEPROM_READ, msg_id, p_data) != 0x01)
        {
            Error_Handler(__FILE__, __LINE__);
        }
        RE_CAN_Format_TxMsg(RX_NODE_FRIGG, msg_id, p_data); /* Format and Enqueue Tx Msg */
        break;
    case 0x02:
        /* Cause: Received new Dock ID and Host ID*/
        /* Effect: Update Dock ID and Host ID */
        RE_CAN_EEPROM_Msg(EEPROM_WRITE, msg_id, p_data);
        sys_var.dock_id = p_data[1];
        break;
    case 0x03:
        /* Cause: Request Available energy */
        /* Effect: Send battery available energy to FRIGG */
        if (RE_SOC_GetData(2, p_data) == 0x01)
        {
            Error_Handler(__FILE__, __LINE__);
        }
        RE_CAN_Format_TxMsg(RX_NODE_FRIGG, msg_id, p_data); /* Format and Enqueue Tx Msg */
        break;
    
    default:
        break;
    }
}

void RE_CAN_ODIN_Msg(uint8_t msg_id, uint8_t *p_data)
{
  
   if(socTrackingEnabled != 1){
        
        socTrackingEnabled =1;
        static uint8_t buffer[3] = {0};
        // Read Gauge data and update to EEPROM and Also in global variable "SOC_TrackingData"
        if (RE_SOC_GetData(5, buffer) == 0x01) /* Read SOC */
        {
            Error_Handler(__FILE__, __LINE__);
        }
        SOC_TrackingData = buffer[1];
        Consumed_SOC = 0;
        
        buffer[0] = 0x01;  		/* SOC Tracking Flag */
        RE_EEPROM_Write(&buffer[0], 1, SOC_TRACKING_FLAG_ADDR); /* Write 1 byte */ 
        
        buffer[0] = SOC_TrackingData; 	/* SOC Avail Data */		/* Consumed SOC */	
        RE_EEPROM_Write(&buffer[0], 1, SOC_AVAIL_DATA_ADDR);  /* Write 2 bytes */
        
        buffer[0] = 0x00; 	/* Consumed SOC */	
        RE_EEPROM_Write(&buffer[0], 1, CONSUMED_SOC_ADDR);  /* Write 2 bytes */  
        HAL_GPIO_WritePin(CHG_INH_GPIO_Port, CHG_INH_Pin, GPIO_PIN_SET);
    }
   
    /* p_data[0] = DLC */
    /* p_data[1:8] = CAN data */
    switch (msg_id)
    {
    case 0x00: /*  */
        /* Cause: Request system reset */
        /* Effect: perform soft reset on the system */
        HAL_NVIC_SystemReset();
        break;
    case 0x01: /* Cause: Request Physical ID */
    case 0x02:
    case 0x03:
    case 0x04:
    case 0x05:
    case 0x06:
    case 0x07:
    case 0x08:
        if (RE_CAN_EEPROM_Msg(EEPROM_READ, msg_id, p_data) != 0x01)
        {
            Error_Handler(__FILE__, __LINE__);
        }
        break;
    case 0x09:
        /* Cause: Request for Gauge Data short packet */
        /* Effect: Send 1 packet of gauge data */
        if (RE_SOC_GetData(1, p_data) == 0x01)
        {
            Error_Handler(__FILE__, __LINE__);
        }
        break;
    case 0x0A:
        /* Cause: Request for Available Energy */
        /* Effect: Send 1 packet of available energy in battery */
        if (RE_SOC_GetData(2, p_data) == 0x01)
        {
            Error_Handler(__FILE__, __LINE__);
        }
        break;
    case 0x0B: /* TODO: */
        /* Cause: Request for Gauge Data long packet */
        /* Effect: Send multiple packets of gauge data */
        break;
    default:
        return;
        break;
    }
    RE_CAN_Format_TxMsg(RX_NODE_ODIN, msg_id, p_data); /* Format and Enqueue Tx Msg */
}

void RE_CAN_BIFROST_Msg(uint8_t msg_id, uint8_t *p_data)
{
   /*** disable Tracking SOC */
    if(socTrackingEnabled != 2){
        socTrackingEnabled =2;
        static uint8_t buffer[1] = {0x02};
        RE_EEPROM_Write(&buffer[0], 1, SOC_TRACKING_FLAG_ADDR);
//        HAL_GPIO_WritePin(CHG_INH_GPIO_Port, CHG_INH_Pin, GPIO_PIN_RESET);
    }
    
    /* p_data[0] = DLC */
    /* p_data[1:8] = CAN data */
    uint16_t Last_AvlEnergy, Current_AvlEnergy, Consmued_Energy;
    switch (msg_id)
    {
        case 0x00: /*  */
            /* Cause: Received new Dock ID and Host ID*/
            /* Effect: Update Dock ID and Host ID */
            RE_CAN_EEPROM_Msg(EEPROM_WRITE, 0x02, p_data);
            sys_var.dock_id = p_data[1];
            break;
        case 0x01: /* Cause: Request Physical ID */
        case 0x02:
        case 0x03:
        case 0x04:
        case 0x05:
        case 0x06:
        case 0x07:
        case 0x08:
            if (RE_CAN_EEPROM_Msg(EEPROM_READ, msg_id, p_data) != 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }
            break;
        case 0x09:
            /* Cause: Request for Gauge Data short packet */
            /* Effect: Send 1 packet of gauge data */
            if (RE_SOC_GetData(1, p_data) == 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }
            break;
        case 0x0A:
          {
            /* Cause: Request for Available Energy */
            /* Effect: Send 1 packet of available energy in battery */
            uint16_t avl_energy = 0, stored_avl_energy = 0;
            uint8_t avl_soc = 0, stored_soc = 0;
            
            if (RE_SOC_GetData(2, p_data) == 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }
            avl_energy = ((p_data[2] << 8) | p_data[1]);           
            RE_CAN_EEPROM_Msg(EEPROM_WRITE, 0x09, p_data);
            RE_CAN_EEPROM_Msg(EEPROM_READ, 0x09, p_data);
            stored_avl_energy = ((p_data[2] << 8) | p_data[1]);
            
            if (RE_SOC_GetData(5, p_data) == 0x01) /* Read SOC */
            {
                Error_Handler(__FILE__, __LINE__);
            }
            avl_soc = p_data[1];           
            RE_CAN_EEPROM_Msg(EEPROM_WRITE, 0x0B, p_data);
            RE_CAN_EEPROM_Msg(EEPROM_READ, 0x0B, p_data);
            stored_soc = p_data[1];
            
            p_data[0] = 0x01;
            if((avl_energy == stored_avl_energy) && (avl_soc == stored_soc))
            {
                p_data[1] = 0x02;
            }
            else
            {
                p_data[1] = 0x01;
            }
          }
//            return;
            break;
        case 0x0B: /* TODO: */
            /* Cause: Request for Gauge Data long packet */
            /* Effect: Send multiple packets of gauge data */
            break;
        case 0x0C:
            /* Cause: Request system reset */
            /* Effect: perform soft reset on the system */
            HAL_NVIC_SystemReset();
            break;
        case 0x0D:
            if (RE_CAN_EEPROM_Msg(EEPROM_READ, 0x09, p_data) != 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }
            break; 
        case 0x0E:
            {
                int current;
                if (RE_SOC_GetData(1, p_data) == 0x01)
                {
                    Error_Handler(__FILE__, __LINE__);
                }
                p_data[0] = 0x04;    //DLC
                current = (int)(((p_data[4] << 8) | p_data[3]) * 0.002);
                if((p_data[1] > 95) & ((p_data[5] >= 20) & (p_data[5] <= 35)) & ((p_data[6] >= 20) & (p_data[6] <= 35))) //p_data[1] = SoC,  p_data[5] = Gauge Internal Temp  , p_data[6] = Ext. Temp 1
                {
                    sys_var.swap_state = 1;
                }
                else
                {
                    sys_var.swap_state = 0;           
                }
                if((p_data[1] < 95) & ((p_data[5] >= 15) & (p_data[5] <= 35)) & ((p_data[6] >= 15) & (p_data[6] <= 35))) //p_data[1] = SoC < 95,  p_data[5] = Gauge Internal Temp, p_data[6] = Ext. Temp 1
                {
                    sys_var.charge_state1 = 1;
                }
                else
                {
                    sys_var.charge_state1 = 0;
                }
                if((p_data[1] >= 99) | ((p_data[5] <= 15) | (p_data[5] >= 45)) | ((p_data[6] <= 15) | (p_data[6] >= 45))) /* p_data[1] = SoC >= 98,, Check ranges for Gauge Internal Temperature (p_data[5]) and External temp (p_data[6])*/
                {
                    sys_var.charge_state2 = 1;
                }
                else
                {
                    sys_var.charge_state2 = 0;      
                }
                if(current > 0)
                {
                    p_data[4] = 0x01;
                }
                else
                {
                    p_data[4] = 0x00;
                }
                p_data[1] = sys_var.swap_state;
                p_data[2] = sys_var.charge_state1;
                p_data[3] = sys_var.charge_state2;
            }
            break;
        case 0x0F:
            if (RE_SOC_GetData(4, p_data) == 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }            
            break;  
        case 0x10:    //host id
            RE_CAN_EEPROM_Msg(EEPROM_WRITE, 0x0A, p_data);
            break;
        case 0x11:{
            if (RE_CAN_EEPROM_Msg(EEPROM_READ, 0x09, p_data) != 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }
            Last_AvlEnergy = ((p_data[2] << 8) | p_data[1]);
            if (RE_SOC_GetData(2, p_data) == 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }
            Current_AvlEnergy = ((p_data[2] << 8) | p_data[1]);
            Consmued_Energy = Last_AvlEnergy - Current_AvlEnergy;
            
            if(Current_AvlEnergy > Last_AvlEnergy)
            {
                Consmued_Energy = 0;
            }
            
            /* Calculating SOC */
            static uint8_t LastStored_Soc =0, Current_Soc =0,Consumed_Soc =0;
            if (RE_CAN_EEPROM_Msg(EEPROM_READ, 0x0B, p_data) != 0x01) /* Read SOC */
            {
                Error_Handler(__FILE__, __LINE__);
            }
            LastStored_Soc = p_data[1];
            
            if (RE_SOC_GetData(5, p_data) == 0x01)
            {
                Error_Handler(__FILE__, __LINE__);
            }
            
            Current_Soc = p_data[1];
            Consumed_Soc = LastStored_Soc - Current_Soc ;
            
            if(Current_Soc > LastStored_Soc){
                p_data[4] = 0x01; /* Raise a flag */
                Consumed_Soc = Current_Soc - LastStored_Soc ;
            }
            else{
                p_data[4] = 0x00;
            }
            
            if(Consumed_Soc > 100){
                Consumed_Soc = 100;
            }
            
            p_data[0] = 5;
            p_data[2] = (uint8_t)(Consmued_Energy >> 8);
            p_data[1] = (uint8_t)(Consmued_Energy);
            p_data[3] = Consumed_Soc;
            
            static uint8_t socTrackingData[2]= {0};
            RE_EEPROM_Read(socTrackingData, CONSUMED_SOC_LEN, CONSUMED_SOC_ADDR); /* Read 1 byte of Consumed SOC */
            p_data[5]  = socTrackingData[0];
            
            break;
        }
        case 0x12:
            RE_CAN_EEPROM_Msg(EEPROM_READ, 0x0A, p_data);
            break;
        case 0x13:
            HAL_GPIO_WritePin(CHG_INH_GPIO_Port, CHG_INH_Pin, (GPIO_PinState)(p_data[1] - 1));
            return;
            break;
        case 0x14:{ /* Dummy data write on Avail Energy and SOC EEprom address */
            static uint8_t writeValuesEepromSOC[2] ={0};
            static uint8_t writeValuesEepromAvailEnergy[7] ={0};
            writeValuesEepromSOC[1] = p_data[1]; /* SOC to be written */
            memcpy(&writeValuesEepromAvailEnergy[1],&p_data[2],6);  /* Avail Energy to be written {LSB, MSB}*/
            RE_CAN_EEPROM_Msg(EEPROM_WRITE, 0x0B, writeValuesEepromSOC);
            RE_CAN_EEPROM_Msg(EEPROM_WRITE, 0x09, writeValuesEepromAvailEnergy);
            break;
          }
        case 0x15:{
            p_data[0] = 0x03 ; /* DLC */
            uint8_t writeValuesEepromSOC[2] ={0};
            uint8_t writeValuesEepromAvailEnergy[7] ={0};
            memset(&p_data[1],0x00,3);          /* Avail Energy to be written {LSB, MSB}*/
            RE_CAN_EEPROM_Msg(EEPROM_READ, 0x0B, writeValuesEepromSOC);         /* SOC */
            RE_CAN_EEPROM_Msg(EEPROM_READ, 0x09, writeValuesEepromAvailEnergy);  /* AvailEnergy */ 
            p_data[1]= writeValuesEepromAvailEnergy[1]; // LSB
            p_data[2]= writeValuesEepromAvailEnergy[2]; // MSB
            p_data[3]= writeValuesEepromSOC[1];
            break;
          }
        default:
            return;
    }
    RE_CAN_Format_TxMsg(RX_NODE_BIFROST, msg_id, p_data); /* Format and Enqueue Tx Msg */
}

void RE_CAN_WriteMsg(uint8_t *p_tx_msg)
{
    /* p_tx_msg[0] = can_id[3] */
    /* p_tx_msg[1] = can_id[2] */
    /* p_tx_msg[2] = can_id[1] */
    /* p_tx_msg[3] = can_id[0] */
    /* p_tx_msg[4] = DLC */
    /* p_tc_msg[5:12] = CAN DATA */
    uint32_t tx_mail_box;
    uint32_t can_id = (p_tx_msg[0] << 24) | (p_tx_msg[1] << 16) | (p_tx_msg[2] << 8) | (p_tx_msg[3]);
    TxHeader.DLC = p_tx_msg[4];
    TxHeader.ExtId = can_id;
    TxHeader.IDE = CAN_ID_EXT;
    TxHeader.RTR = CAN_RTR_DATA;
    if (TxHeader.DLC < 9)
    {
        /* FIXME: Make use of CAN_TX_Empty INT and change polling mode */
        while (HAL_CAN_GetTxMailboxesFreeLevel(&hcan1) == 0)
        {
        }
        if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, &p_tx_msg[5], &tx_mail_box) != HAL_OK)
        {
            Error_Handler(__FILE__, __LINE__);
        }
    }
}

void RE_CAN_Format_TxMsg(uint8_t dest_node_id, uint8_t msg_id, uint8_t *p_data)
{
    uint8_t msg_item[TxQueueItemLen] = {0};
    msg_item[0] = Tx_NODE_BATTERY >> 8;                      /* CAN_ID[3] */
    msg_item[1] = (uint8_t)(Tx_NODE_BATTERY | dest_node_id); /* CAN_ID[2] */
    msg_item[2] = sys_var.dock_id;                           /* CAN_ID[1] */
    msg_item[3] = msg_id;                                    /* CAN_ID[0] */
    msg_item[4] = p_data[0];                                 /* DLC */
    for (uint8_t i = 0; i < msg_item[4]; i++)
    {
        msg_item[5 + i] = p_data[1 + i];
    }
    RE_EnQueue(p_CanTxQueue, msg_item, TxQueueItemLen);
}

/**
  * @brief RE_CAN1_Filter_Config
  *  This function configures CAN1 filter banks
  * @param None
  * @retval None
  */
static void RE_CAN1_Filter_Config(void)
{
    CAN_FilterTypeDef CAN1_Filter_t;

    /* L2 Filter: 0x00A1xxxx - TO Battery EEPROM */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 0; /* FilterBank:0 */
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0508;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFF8;
    CAN1_Filter_t.FilterMaskIdLow = 0x0004;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }

    /* L1 Filter: 0x00B4xxxx - Frigg to Battery */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 1; /* FilterBank:2 */
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x05A0;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFF8;
    CAN1_Filter_t.FilterMaskIdLow = 0x0004;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }

    /* L3 Filter: 0x00C3xxxx - Odin to Battery */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 2;
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0618;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFF8;
    CAN1_Filter_t.FilterMaskIdLow = 0x0004;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    
    /* L4 Filter: 0x00E3xxxx - Biforst to Battery */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 3;
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0718;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFF8;
    CAN1_Filter_t.FilterMaskIdLow = 0x0004;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }    
}

/**
  * @brief RE_CAN1_Start_Interrupt
  *  This function switches CAN1 from initialisation mode to start mode
  * @param None
  * @retval None
  */
static void RE_CAN1_Start_Interrupt(void)
{
    if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_TX_MAILBOX_EMPTY | CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_LAST_ERROR_CODE |
                                                 CAN_IT_RX_FIFO1_MSG_PENDING | CAN_IT_BUSOFF | CAN_IT_ERROR | CAN_IT_ERROR_PASSIVE | CAN_IT_ERROR_WARNING) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /* CAN enter normal operation mode*/
    if (HAL_CAN_Start(&hcan1) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
}

