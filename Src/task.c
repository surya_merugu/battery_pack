#include "task.h"
#include "string.h"

extern sys_var_t sys_var;
extern Queue_t *p_CanRxQueue;
extern Queue_t *p_CanTxQueue;
extern IWDG_HandleTypeDef hiwdg;
extern bool reset_iwdg_Flag;
extern CAN_HandleTypeDef hcan1;
extern uint8_t socTrackingEnabled , SOC_TrackingData,Consumed_SOC;
bool RE_Task_Pending(void)
{
    bool ret_val = false;
    if (RE_IsQueueEmpty(p_CanRxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_CanRxMsg = RE_DeQueue(p_CanRxQueue, RxQueueItemLen);
        /* p_CanRxMsg[0:3] = { 29-bit CAN ID } */
        /* p_CanRxMsg[4] = DLC */
        uint8_t msg_id = p_CanRxMsg[3];
        uint8_t dock_id = p_CanRxMsg[2];
        uint8_t peripheral_id = (p_CanRxMsg[1] & 0x0F);
        uint8_t node_id = (p_CanRxMsg[1] >> 4);
        uint8_t mode = 0;

        switch (node_id)
        {
        case 0x0A:                        /* Data to/from Battery EEPROM */
            switch (p_CanRxMsg[1] & 0x0F) /* 2nd byte of CAN ID */
            {
            case 0x01: /* WR or RD to/from EEPROM [0x00A1xx01] */
                mode = (p_CanRxMsg[4] != 0) ? EEPROM_WRITE : EEPROM_READ;
                RE_CAN_EEPROM_Msg(mode, msg_id, &p_CanRxMsg[4]); /* p_CanRxMsg[0] = msg_id; p_CanRxMsg[4] = DLC */
                if (mode == EEPROM_READ)
                {
                    RE_CAN_Format_TxMsg(NODE_BAT_EEPROM, msg_id, &p_CanRxMsg[4]); /* Format and Enqueue Tx Msg */
                }
                break;
            default:
                break;
            }
            break;
        case 0x0B: /* CAN Message from FRIGG [0x00B2XX01] */
            if ((peripheral_id == 0x04) && ((dock_id == 0) || (dock_id == sys_var.dock_id)))
            {
                RE_CAN_FRIGG_Msg(msg_id, &p_CanRxMsg[4]); /* p_CanRxMsg[4] = DLC */
            }
            break;
        case 0x0C: /*  CAN Message from ODIN [0x00C3XX01] */
            if ((peripheral_id == 3) && ((dock_id == 0) || (dock_id == sys_var.dock_id)))
            {
                RE_CAN_ODIN_Msg(msg_id, &p_CanRxMsg[4]); /* p_CanRxMsg[4] = DLC */
            }
            break;
        case 0x0E:   /** CAN Message from BIFROST {0x00E3xxxx}*/
            if((peripheral_id == 0x03) && ((dock_id == 0) || (dock_id == sys_var.dock_id)))
            {
                RE_CAN_BIFROST_Msg(msg_id, &p_CanRxMsg[4]);
            }
            break;
        default:
            break;
        }
    }
    if (RE_IsQueueEmpty(p_CanTxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_CanTxMsg = RE_DeQueue(p_CanTxQueue, TxQueueItemLen);
        RE_CAN_WriteMsg(p_CanTxMsg);
    }
    if(reset_iwdg_Flag)
    {
        reset_iwdg_Flag = false;
        if(HAL_IWDG_Refresh(&hiwdg) != HAL_OK)
        {
            Error_Handler(__FILE__, __LINE__);
        }
    }
    return ret_val;
}

void RE_load_system_status(void)
{
    uint8_t buffer[5] = {0};
    RE_EEPROM_Read(buffer, 5, DOCK_ID_ADDR); /* Read 5 bytes from Dock ID */
    sys_var.dock_id = buffer[0];
    sys_var.host_id = buffer[1];
    sys_var.rcps_mode = buffer[2];
    sys_var.g2g_status = buffer[3];
    sys_var.tamper_status = buffer[4];
    
    memset(buffer, 0x00, 5);
    RE_EEPROM_Read(buffer, 1, SOC_TRACKING_FLAG_ADDR); /* Read 1 byte from SOC tracking Mem Address */
    socTrackingEnabled = buffer[0];
    HAL_Delay(1);
    /** Initialization condition for SOC tracking 
    *	socTrackingEnabled : value 1 indicates Battery presence in KIT
    *	socTrackingEnabled : value 2 indicates Battery presence in BIFROST
    */
    if( (socTrackingEnabled == 1) || (socTrackingEnabled == 2) ){
        //HAL_Delay(5);
        memset(buffer, 0x00, 5);
        RE_EEPROM_Read(buffer, 1, SOC_AVAIL_DATA_ADDR); /* Read 2 byte from SOC DATA and Consumed SOC Mem Address */
        SOC_TrackingData = buffer[0];
        
        RE_EEPROM_Read(buffer, 1, CONSUMED_SOC_ADDR); /* Read 2 byte from SOC DATA and Consumed SOC Mem Address */
        Consumed_SOC = buffer[0];
    }
   else if( (socTrackingEnabled != 1) && (socTrackingEnabled != 2) ){
        //HAL_Delay(5);   
        memset(buffer, 0x00, 5);
        RE_EEPROM_Write(buffer, 1, SOC_TRACKING_FLAG_ADDR); /* Clear SOC Tracking Flag, SOC_TrackingData and Consumed SOC Mem Address */
        
        RE_EEPROM_Write(buffer, 1, SOC_AVAIL_DATA_ADDR); /* Clear SOC Tracking Flag, SOC_TrackingData and Consumed SOC Mem Address */
        
        RE_EEPROM_Write(buffer, 1, CONSUMED_SOC_ADDR); /* Clear SOC Tracking Flag, SOC_TrackingData and Consumed SOC Mem Address */
        socTrackingEnabled = 0;
    }
    else{
        /* Ignore for bifrost */
    }
}