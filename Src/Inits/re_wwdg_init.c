/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_wwdg_init.c
  * Origin Date           :   08/05/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, MAY 2020
  *****************************************************************************
  */

/* Includes */
#include "re_wwdg_init.h"

WWDG_HandleTypeDef hwwdg_t;

/**
 * @brief Timer Base Init
 * This function configures clock and set priority for the timer
 * @param  htim3_t: timer handle typedef
 * @retval Exit status
 */
RE_StatusTypeDef RE_WWDG_Init(void)
{  
      __HAL_RCC_WWDG_CLK_ENABLE();
      hwwdg_t.Instance          = WWDG;
      hwwdg_t.Init.Prescaler    = WWDG_PRESCALER_8;
      hwwdg_t.Init.Window       = 127;
      hwwdg_t.Init.Counter      = 127;
      hwwdg_t.Init.EWIMode      = WWDG_EWI_ENABLE;
      if (HAL_WWDG_Init(&hwwdg_t) != HAL_OK)
      {
        RE_Error_Handler(__FILE__, __LINE__);
      }
      HAL_NVIC_SetPriority(WWDG_IRQn, 0, 0);
      HAL_NVIC_EnableIRQ(WWDG_IRQn);
      return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/
