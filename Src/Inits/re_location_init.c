/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_location_init.c
  * Origin Date           :   25/05/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, MAY 2020
  *****************************************************************************
  */

/* Includes */
#include "re_location_init.h"

/**
 * @brief  Latch GPIO Initialization
 * This function configures the GPIO pins used by  Latch
 * @param None
 * @retval Exit status
 */
RE_StatusTypeDef RE_Location_GpioInit (void)
{
    GPIO_InitTypeDef LocationPin;
    __HAL_RCC_GPIOA_CLK_ENABLE();

     LocationPin.Pin               = GPIO_PIN_7;
     LocationPin.Mode              = GPIO_MODE_INPUT;
     LocationPin.Speed             = GPIO_SPEED_FAST;
     LocationPin.Pull              = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOA, &LocationPin);

     return RE_OK;
}

