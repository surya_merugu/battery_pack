/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_rcps_init.c
  * Origin Date           :   15/06/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, JUN 2020
  *****************************************************************************
  */

/* Includes */
#include "re_rcps_init.h"

/**
 * @brief  Latch GPIO Initialization
 * This function configures the GPIO pins used by  Latch
 * @param None
 * @retval Exit status
 */
RE_StatusTypeDef RE_rcps_GpioInit (void)
{
    GPIO_InitTypeDef ChargeFET, DischargeFET;
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

    /**
    * PC9 : Charge FET
    * PD11 : Discharge FET
    */

     ChargeFET.Pin               = GPIO_PIN_11;
     ChargeFET.Mode              = GPIO_MODE_OUTPUT_PP;
     ChargeFET.Speed             = GPIO_SPEED_FAST;
     ChargeFET.Pull              = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOD, &ChargeFET);

     DischargeFET.Pin            = GPIO_PIN_9;
     DischargeFET.Mode           = GPIO_MODE_OUTPUT_PP;
     DischargeFET.Speed          = GPIO_SPEED_FAST;
     DischargeFET.Pull           = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOC, &DischargeFET);
     return RE_OK;
}

