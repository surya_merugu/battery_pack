/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_iwdg_init.c
  * Origin Date           :   08/05/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, MAY 2020
  *****************************************************************************
  */

/* Includes */
#include "re_iwdg_init.h"

IWDG_HandleTypeDef hiwdg_t;

/**
 * @brief independent watchdog timer init
 * This function configures clock and set priority for the timer
 * @param  htim3_t: timer handle typedef
 * @retval Exit status
 */
RE_StatusTypeDef RE_IWDG_Init(void)
{
      hiwdg_t.Instance       = IWDG;
      hiwdg_t.Init.Prescaler = IWDG_PRESCALER_128;
      hiwdg_t.Init.Reload    = 4095;
      if (HAL_IWDG_Init(&hiwdg_t) != HAL_OK)
      {
        RE_Error_Handler(__FILE__, __LINE__);
      }
      return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/
