/**
  ******************************************************************************
  * @file           : gauge.c
  * @brief          : Bq34z100 peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "gauge.h"
#include "math.h"

I2C_HandleTypeDef hi2c1;
extern uint8_t socTrackingEnabled , SOC_TrackingData, Consumed_SOC;
__IO uint8_t rx_cplt = 0;

#if 0
static DataBlock_config_t RE_SoC_ConfigDataBlock(uint8_t block_num);
#endif
static RE_StatusTypeDef RE_SoC_I2CReadDataBlock(uint8_t gauge_mem_addr, uint8_t gauge_mem_len, void *p_buff_addr);

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
void RE_I2C1_Init(void)
{

    /* USER CODE BEGIN I2C1_Init 0 */

    /* USER CODE END I2C1_Init 0 */

    /* USER CODE BEGIN I2C1_Init 1 */

    /* USER CODE END I2C1_Init 1 */
    hi2c1.Instance = I2C1;
    hi2c1.Init.ClockSpeed = 100000;
    hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
    hi2c1.Init.OwnAddress1 = 0;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK)
    {
        Error_Handler(__FILE__, __LINE__);
    }
    /* USER CODE BEGIN I2C1_Init 2 */

    /* USER CODE END I2C1_Init 2 */
}

uint8_t RE_SOC_GetData(uint8_t read_type, uint8_t *p_can_soc_packet)
{

    uint16_t *p_ext_temp_buff;
    uint8_t buffer[9] = {0};
    uint8_t volt, temperature = 0;
    uint8_t gauge_read_addr = 0;
    switch (read_type)
    {
        case 1: /* Kit cyclic msg [soc, volt, AvgCurrent, Internal Temp, Ext Temp1, Ext Temp2] */{
            gauge_read_addr = 0x02;
            RE_SoC_I2CReadDataBlock(gauge_read_addr, 1, &buffer[0]); /* Read SoC (len = 1 byte) */
            gauge_read_addr = 0x08;
            RE_SoC_I2CReadDataBlock(gauge_read_addr, 6, &buffer[1]); /* Read Volt, AvgCurrent, Temperature (len = 6 bytes) */
            /*  */
            gauge_read_addr = 0x10;
            RE_SoC_I2CReadDataBlock(gauge_read_addr, 2, &buffer[3]); /* Read Inst. Current (len = 2 byte) */

            /* convert voltage and temp values to usable values */
            volt = (uint8_t)(((buffer[2] << 8 | buffer[1]) / 50) - 350); /* V(tx) = [V(soc)/50]-350 mvolt  Range = [35.0 : 60.5]volt */
            temperature = (uint8_t)(round(((buffer[6] << 8 | buffer[5]) * 0.1) - 273.15));

            gauge_read_addr = 0x24;
            RE_SoC_I2CReadDataBlock(gauge_read_addr, 2, &buffer[7]); /* Read Avl Energy (len = 2 bytes) */
            
            p_ext_temp_buff = RE_ADC_Get_TempSensor_Data();    /* Read sensors adc data */
            p_can_soc_packet[0] = 0x08;                        /* DLC */
            p_can_soc_packet[1] = buffer[0];                   /* SoC */
            p_can_soc_packet[2] = volt;                        /* VOlt */
            p_can_soc_packet[3] = buffer[3];                   /* Inst/Avg.Current LSB */
            p_can_soc_packet[4] = buffer[4];                   /* Inst/Avg.Current MSB */
            p_can_soc_packet[5] = temperature;                 /* Gauge Temp sensor */
            p_can_soc_packet[6] = (uint8_t)p_ext_temp_buff[3]; /* Ext temperature sensor 1 */
            p_can_soc_packet[7] = buffer[7];                   /* Avail ENergy MSB */
            p_can_soc_packet[8] = buffer[8];                   /* Avail ENergy LSB */
            
            uint8_t CurrentSOC = buffer[0]; 
            if( (socTrackingEnabled ==1) && (SOC_TrackingData > CurrentSOC) && (CurrentSOC > 0) && (CurrentSOC <= 100) ){
                Consumed_SOC += (SOC_TrackingData - CurrentSOC);
                SOC_TrackingData = CurrentSOC;
                static uint8_t socData[2] = {0};
                socData[0] = CurrentSOC; 	/* SOC Avail Data */	
                RE_EEPROM_Write(socData, 1, SOC_AVAIL_DATA_ADDR); /* Write 1 bytes */
                
                socData[0] = Consumed_SOC; 	/* Consumed SOC */
                RE_EEPROM_Write(socData, 1, CONSUMED_SOC_ADDR);  /* Write 1 bytes */ 
            }
        }
        break;
    case 2:
            gauge_read_addr = 0x24;
            RE_SoC_I2CReadDataBlock(gauge_read_addr, 2, &buffer[0]); /* Read Avl Energy (len = 2 bytes) */
            p_can_soc_packet[0] = 0x02;                              /* DLC */
            p_can_soc_packet[1] = buffer[0];                         /* Avl. Engy LSB */
            p_can_soc_packet[2] = buffer[1];                         /* Avl. Engy MSB */
            break;
    case 3: /* Read 46 bytes of gauge data */
    {
#if 0
            uint8_t i = 0;
            static uint8_t gauge_buffer[GAUGE_BUFF_LEN];
            uint8_t *p_buff_addr = &gauge_buffer[0];
            while (i < 4)
            {
                DataBlock_config_t gauge_mem;
                gauge_mem = RE_SoC_ConfigDataBlock(i); /* Configure blocks first */
                RE_SoC_I2CReadDataBlock(gauge_mem.Addr, gauge_mem.Len, p_buff_addr);
                p_buff_addr = (p_buff_addr + gauge_mem.Len);
                i++;
            }
            return gauge_buffer;
#endif
            break;
    }
    case 4:
            gauge_read_addr = 0x2C;
            RE_SoC_I2CReadDataBlock(gauge_read_addr, 2, &buffer[0]); /* Read Cycle Count (len = 2 bytes) */
            p_can_soc_packet[0] = 0x02;                              /* DLC */
            p_can_soc_packet[1] = buffer[0];                         /* Cycle Count LSB */
            p_can_soc_packet[2] = buffer[1];                         /* Cycle Count MSB */
            break;
    case 5: /* Read SOC  */{
            gauge_read_addr = 0x02;
            RE_SoC_I2CReadDataBlock(gauge_read_addr, 1, &buffer[0]); /* Read SoC (len = 1 byte) */
            p_can_soc_packet[0] = 0x01;                        /* DLC */
            p_can_soc_packet[1] = buffer[0];                   /* SoC */
            
            break;
    }
    default:
        return 1;
        break;
    }
    return 0;
}

#if 0
static DataBlock_config_t RE_SoC_ConfigDataBlock(uint8_t block_num)
{
    DataBlock_config_t block;
    switch (block_num)
    {
    case 0:
        /* Block: 0 */
        block.Addr = 0x02;
        block.Len = 18;
        break;
    case 1:
        /* Block: 1 */
        block.Addr = 0x18;
        block.Len = 8;
        break;
    case 2:
        /* Block: 2 */
        block.Addr = 0x24;
        block.Len = 16;
        break;
    case 3:
        /* Block: 3 */
        block.Addr = 0x68;
        block.Len = 4;
        break;
    default:
        /**To Do**/
        break;
    }
    return block;
}
#endif


/**
  * @brief  Read Gauge data blocks into array
  * @param  block_num : Block Number
  * @retval None  
  */
static RE_StatusTypeDef RE_SoC_I2CReadDataBlock(uint8_t gauge_mem_addr, uint8_t gauge_mem_len, void *p_buff_addr)
{
    /* Wait until I2C state is ready */
    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
    {
    }
    /* Master write address to Slave */
    while (HAL_I2C_Master_Transmit_DMA(&hi2c1, BQ_WR_ADDR, &gauge_mem_addr, sizeof(gauge_mem_addr)) != HAL_OK)
    {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
        {
            Error_Handler(__FILE__, __LINE__);
        }
    }
    /* Wait until I2C state is ready */
    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
    {
    }
    rx_cplt = 0; /* Reset flag */
    /* Master request Slave for data */
    while (HAL_I2C_Master_Receive_DMA(&hi2c1, BQ_RD_ADDR, p_buff_addr, gauge_mem_len) != HAL_OK)
    {
        if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
        {
            Error_Handler(__FILE__, __LINE__);
        }
    }
    while (!rx_cplt)
    {
    }
    return RE_OK;
}

void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
}

void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    rx_cplt = 1;
}
