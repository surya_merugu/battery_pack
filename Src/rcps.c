#include "rcps.h"

void RE_RCPS_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Port Clock Enable */
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, CHG_INH_Pin | DSG_INH_Pin | RCPS_IO_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pins : CHG_INH_Pin DSG_INH_Pin RCPS_IO_Pin */
    GPIO_InitStruct.Pin = CHG_INH_Pin | DSG_INH_Pin | RCPS_IO_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void RE_RCPS_SetMode(uint8_t mode)
{
    HAL_GPIO_WritePin(GPIOB, CHG_INH_Pin, (GPIO_PinState)mode);
#if 0
    HAL_GPIO_WritePin(GPIOB, DSG_INH_Pin, (GPIO_PinState)mode);
#endif
}