/**
  ******************************************************************************
  * @file           : gpio.c
  * @brief          : GPIO peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "gpio.h"

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
void RE_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    /* Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOD, STATUS_LED, GPIO_PIN_RESET);

    /*Configure GPIO pins : CAN1_ERR_Pin MEMS_INTN_Pin */
    GPIO_InitStruct.Pin = MEMS_INTN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* Configure GPIO pin : LOC_Pin */
    GPIO_InitStruct.Pin = LOC_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(LOC_GPIO_Port, &GPIO_InitStruct);

    /* Configure GPIO pins : STATUS_LED */
    GPIO_InitStruct.Pin = STATUS_LED;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    /* USER CODE BEGIN 4 */

    /* USER CODE END 4 */
}