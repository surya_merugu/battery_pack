#ifndef __RE_QUEUE_H
#define __RE_QUEUE_H

#include "main.h"
#include "limits.h"
#include "stdlib.h"

/* A structure to represent a queue */
typedef struct Queue
{
    uint8_t first, last, capacity, size;
    uint8_t *p_array;
} Queue_t;

Queue_t *RE_CreateQueue(uint8_t no_of_items, uint8_t item_len);
void RE_DeleteQueue(Queue_t *p_queue_t);
uint8_t RE_IsQueueFull(Queue_t *p_queue_t);
uint8_t RE_IsQueueEmpty(Queue_t *p_queue_t);
uint8_t RE_EnQueue(Queue_t *p_queue_t, uint8_t *p_item, uint8_t len);
uint8_t *RE_DeQueue(Queue_t *p_queue_t, uint8_t len);

#endif