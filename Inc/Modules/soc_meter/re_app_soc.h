/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_app_soc.h
  * Origin Date           :   13/04/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   bq34z100
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, APR 2020
  *****************************************************************************
  */

#ifndef __RE_APP_SOC_H
#define __RE_APP_SOC_H

#include "stm32f4xx_hal.h"
#include "re_std_def.h"
#include "stdlib.h"

#define BQ_WR_ADDR      ((uint8_t) 0xAA)       // I2C address for reading: 7bit address (0x55) + [0] write bit (0xAA)
#define BQ_RD_ADDR      ((uint8_t) 0xAB)       // I2C address for writing: 7bit address (0x55) + [1] read bit  (0xAB) 

#define BLOCK0
#define BLOCK1
#define BLOCK2
#define BLOCK3

extern uint16_t Ext_Tempsensor[3];

/* Gauge Data Block Config Structure */
typedef struct {
  uint8_t Addr;                // Start Address of the DataBlock to read
  uint8_t Len;                 // Length of the DataBlock in bytes to read
}DataBlock_config_t;

/*                  High Priority SoC Data                    */
typedef struct   __attribute__((packed))   
{                                        
	uint16_t StateOfCharge ;
	uint16_t Voltage;
	uint16_t AvgCurrent;
	uint16_t Current;
	uint16_t RemainingCapacity;
        uint16_t Temperature;
        uint16_t AvgTimeToEmpty;
        uint16_t AvgTimeToFull;
        uint16_t PassedCharge;
        uint16_t AvailableEnergy;
        uint16_t AvgPower;
        uint16_t InternalTemperature;
        uint16_t ExternalTemperature; 
        uint16_t  MaxError;
        uint16_t FullChargeCapacity;
        uint16_t SerialNumber;
        uint16_t CycleCount;
        uint16_t StateOfHealth;
        uint16_t TrueRC;     //True Remaining capacity                            
        uint16_t TrueFCC;    //True Full capacity
        uint16_t  LearnedStatus;   //A Soc Meter without errors has Learned Status of 6 always
}gaugeData_t;

extern gaugeData_t gaugeData;

RE_StatusTypeDef RE_SoC_ReadGaugeData (void);

#endif 

/***************************** END OF FILE ************************************/
