#ifndef RE_TIMERS_H
#define RE_TIMERS_H

#include "main.h"

void RE_IWDG_Init(void);
void RE_WWDG_Init(void);
void RE_TIM1_Init(void);
void RE_TIM2_Init(void);
void RE_TIM3_Init(void);

#endif