#ifndef __RE_CAN_H
#define __RE_CAN_H

#include "main.h"
#include "queue.h"
#include "gauge.h"

#define RxQueueItems 8U
#define TxQueueItems 15U
#define RxQueueItemLen 13U /* 4byte can id + 1 byte DLC + 8 byte data */
#define TxQueueItemLen 13U /* 4byte can id + 1 byte DLC + 8 byte data */
#define DEFAULT_CAN_ID (uint32_t)0x1FBA0000

#define Tx_NODE_BATTERY (uint16_t)0x00A0

#define NODE_BAT_EEPROM 0x02
#define RX_NODE_ODIN 0x03
#define RX_NODE_FRIGG 0x04
#define RX_NODE_BIFROST 0x03

#if 0
extern Queue_t *p_CanRxQueue;
extern Queue_t *p_CanTxQueue;
#endif

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
void RE_CAN1_Init(void);

uint8_t RE_CAN_EEPROM_Msg(uint8_t mode, uint8_t msg_id, uint8_t *p_data);
void RE_CAN_Format_TxMsg(uint8_t dest_node_id, uint8_t msg_id, uint8_t *p_data);

void RE_CAN_FRIGG_Msg(uint8_t msg_id, uint8_t *p_data);
void RE_CAN_ODIN_Msg(uint8_t msg_id, uint8_t *p_data);
void RE_CAN_BIFROST_Msg(uint8_t msg_id, uint8_t * p_data);

void RE_CAN_WriteMsg(uint8_t *p_data);
#endif