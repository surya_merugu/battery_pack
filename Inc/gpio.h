#ifndef __RE_GPIO_H
#define __RE_GPIO_H

#include "main.h"

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
extern void RE_GPIO_Init(void);
#endif