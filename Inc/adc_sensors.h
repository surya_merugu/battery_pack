#ifndef __RE_ADC_SENSORS_H
#define __RE_ADC_SENSORS_H

#include "main.h"

#define ADC_SENSORS 5 /* Number of ADC sensors configured */
#define ADC_BUFF_LEN (uint8_t)(ADC_SENSORS * sizeof(uint16_t))

void RE_ADC1_Init(void);
uint16_t *RE_ADC_Get_TempSensor_Data(void);

#endif