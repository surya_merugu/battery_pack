#ifndef __RE_EEPROM_H
#define __RE_EEPROM_H

#include "main.h"

#define EEPROM_WRITE 0x01
#define EEPROM_READ 0x02

#define EEPROM_RW_ADDR (uint16_t)0xAE
#define WAIT_TIME (uint8_t)0x06    /* Wait time = Internal write cycle time on eeprom */
#define MEM_ADD_SIZE (uint8_t)0x02 /* On 0x02 Master will write two bytes of memory addess after device address */

#define PHY_ID_ADDR (uint16_t)0x00
#define PHY_ID_LEN (uint8_t)0x04

#define DOCK_ID_ADDR (uint16_t)0x07
#define DOCK_ID_LEN (uint8_t)0x01

#define RCPS_MODE_ADDR (uint16_t)0x09
#define RCPS_MODE_LEN (uint8_t)0x01

#define G2G_STATE_ADDR (uint16_t)0x0A
#define G2G_STATE_LEN (uint8_t)0x01

#define TAMPER_STATE_ADDR (uint16_t)0x0B
#define TAMPER_STATE_LEN (uint8_t)0x01

#define FW_VERSION_ADDR (uint16_t)0x0C
#define FW_VERSION_LEN (uint8_t)0x02

#define FW_UPDATE_PEND_ADDR (uint16_t)0x0E /* Flag to indicate firmware upgrade status */
#define FW_UPDATE_PEND_LEN (uint8_t)0x01

#define ERR_CODES_ADDR (uint16_t)0x10
#define ERR_CODES_LEN (uint8_t)0x07

#define AVL_ENERGY_ADDR (uint16_t)0x17
#define AVL_ENERGY_LEN  (uint8_t)0x02

#define EPOCH_TIME_ADDR  (uint16_t)0x19
#define EPOCH_TIME_LEN  (uint8_t)0x04

#define HOST_VERIFICATION_ID_ADDR    (uint16_t)0x1D
#define HOST_VERIFICATION_ID_LEN     (uint8_t)0x04

#define SOC_ADDR    (uint16_t)0x24
#define SOC_LEN     (uint8_t)0x01

#define SOC_TRACKING_FLAG_ADDR    (uint16_t)0x27
#define SOC_TRACKING_FLAG_LEN     (uint8_t)0x01

#define SOC_AVAIL_DATA_ADDR       (uint16_t)0x2A
#define SOC_AVAIL_DATA_LEN        (uint8_t)0x01

#define CONSUMED_SOC_ADDR         (uint16_t)0x2B
#define CONSUMED_SOC_LEN          (uint8_t)0x01
/* 
#define GLOBAL_ALERT ERR_CODES_ADDR
#define BAT_ERR (ERR_CODES_ADDR + 1)
#define GAUGE_ERR (ERR_CODES_ADDR + 2)
#define CAN_ERR (ERR_CODES_ADDR + 3)
#define TAMPER_ERR (ERR_CODES_ADDR + 4)
#define TEMPERATURE_ERR (ERR_CODES_ADDR + 5)
#define RCPS_ERR (ERR_CODES_ADDR + 6)
*/

typedef enum
{
    GLOBAL_ALERT = 0,
    BAT_ERR,
    GAUGE_ERR,
    CAN_ERR,
    TAMPER_ERR,
    TEMPERATURE_ERR,
    RCPS_ERR
} ErrCode_t;

void RE_EEPROM_Write(uint8_t *p_data, uint8_t len, uint16_t mem_address);
void RE_EEPROM_Read(uint8_t *p_data, uint8_t len, uint16_t mem_address);

// uint8_t *RE_EEPROM_GetPhyId(void);
// void RE_EEPROM_PutPhyId(void *p_physical_id);

// uint8_t *RE_EEPROM_Get_FwUpgradePend(void);
// void RE_EEPROM_Put_FwUpgradePend(uint8_t *p_fw_upgrade_pend);

// uint8_t *RE_EEPROM_GetCanId(void);
// void RE_EEPROM_PutCanId(uint8_t *p_dock_id);

// uint8_t *RE_EEPROM_GetLoc(void);
// void RE_EEPROM_PutLoc(uint8_t *p_bat_loc);

// uint8_t *RE_EEPROM_GetRcpsMode(void);
// void RE_EEPROM_PutRcpsMode(uint8_t *p_rcps_mode);

// uint8_t *RE_EEPROM_GetFwVersion(void);
// void RE_EEPROM_PutFwVersion(uint8_t *p_fw_version);

// uint8_t *RE_EEPROM_GetG2G(void);
// void RE_EEPROM_PutG2G(uint8_t *p_g2g_status);

// uint8_t *RE_EEPROM_GetTamperStatus(void);
// void RE_EEPROM_PutTamperStatus(uint8_t *p_tamper_status);

// uint8_t *RE_EEPROM_GetErrCode(void);
// void RE_EEPROM_PutErrCode(uint8_t err_type, uint8_t *p_value, uint8_t len);

#endif
