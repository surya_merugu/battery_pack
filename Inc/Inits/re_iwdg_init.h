/**
  *****************************************************************************
  * Title                 :   Bat_Pack_Gen1
  * Filename              :   re_iwdg_init.h
  * Origin Date           :   08/05/2020
  * Compiler              :   Specify compiler used
  * Hardware              :   None 
  * Target                :   STM32F407-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, MAY 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_IWDG_INIT_H
#define _RE_IWDG_INIT_H

/* Includes */
#include "re_std_def.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_iwdg.h"
#include "stm32f4xx_it.h"

extern IWDG_HandleTypeDef hiwdg_t;

/* Exported API */
RE_StatusTypeDef RE_IWDG_Init(void);

#endif
/**************************** END OF FILE *************************************/