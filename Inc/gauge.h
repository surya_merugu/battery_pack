#ifndef __RE_GAUGE_H
#define __RE_GAUGE_H

#include "main.h"
#include "stdlib.h"

#define BQ_WR_ADDR ((uint16_t)0xAA) /* I2C address for writing: 7bit address (0x55) + [0] write bit (0xAA) */
#define BQ_RD_ADDR ((uint16_t)0xAB) /* I2C address for reading: 7bit address (0x55) + [1] read bit  (0xAB) */

#define BLOCK0
#define BLOCK1
#define BLOCK2
#define BLOCK3

#define GAUGE_BUFF_LEN 46 /* 46 Bytes of data will be read from the gauge */

/* Gauge Data Block Config Structure */
typedef struct
{
    uint8_t Addr; /* Start Address of the DataBlock to read */
    uint8_t Len;  /* Length of the DataBlock in bytes to read */
} DataBlock_config_t;

/* Gauge Data Block Config Structure */
#if 0
typedef struct
{
    uint8_t Addr; /* Start Address of the DataBlock to read */
    uint8_t Len; /* Length of the DataBlock in bytes to read */
}
DataBlock_config_t;
#endif

/* High Priority SoC Data */
#if 0
typedef struct __attribute__((packed))
{
    uint16_t StateOfCharge;
    uint16_t Voltage;
    uint16_t AvgCurrent;
    uint16_t Current;
    uint16_t RemainingCapacity;
    uint16_t Temperature;
    uint16_t AvgTimeToEmpty;
    uint16_t AvgTimeToFull;
    uint16_t PassedCharge;
    uint16_t AvailableEnergy;
    uint16_t AvgPower;
    uint16_t InternalTemperature;
    uint16_t ExternalTemperature;
    uint16_t MaxError;
    uint16_t FullChargeCapacity;
    uint16_t SerialNumber;
    uint16_t CycleCount;
    uint16_t StateOfHealth;
    uint16_t TrueRC;        /* True Remaining capacity */
    uint16_t TrueFCC;       /* True Full capacity */
    uint16_t LearnedStatus; /* A Soc Meter without errors has Learned Status of 6 always */
} gaugeData_t;
#endif

void RE_I2C1_Init(void);
uint8_t RE_SOC_GetData(uint8_t read_type, uint8_t *p_can_soc_packet);
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c);

#endif