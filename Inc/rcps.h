#ifndef __RE_RCPS_H
#define __RE_RCPS_H

#include "main.h"

#define CHG_INH_Pin GPIO_PIN_12
#define CHG_INH_GPIO_Port GPIOB
#define DSG_INH_Pin GPIO_PIN_13
#define DSG_INH_GPIO_Port GPIOB
#define RCPS_IO_Pin GPIO_PIN_14
#define RCPS_IO_GPIO_Port GPIOB

void RE_RCPS_Init(void);
void RE_RCPS_SetMode(uint8_t mode);

#endif